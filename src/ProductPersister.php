<?php
/**
 * ProductPersister

 * @author  mrx
 * @version 1.0
 * @package DataImportProductPersister
 * @subpackage ProductPersister
 */

namespace DataImportProductPersister;


use \Core\InputValidate\InputValidate;
use Core\tool\XML2Array;
use Core\Tool\Logging;

/**
 * Class ProductPersister
 *
 * Transform data structure
 * from Xml to array
 *
 * @author  mrx
 * @package    DataImportProductPersister
 * @subpackage ProductPersister
 */
class ProductPersister
{
    /**
     * Required fields
     * @var array $config
     */
    private $configKeys = ['arrayToProcess'];
    private $mongoKeys = ['mongoServer','mongoPort','mongoDBName','mongoCollectionName'];

    /**
     * Persist array into mongodb.
     * @param array $config
     */
    public function persist(array $config)
    {

        // Input validation
        InputValidate::config($config, $this->configKeys);
        // Input validation for Mongo
        InputValidate::config($config, $this->mongoKeys);


        // Input validation
        InputValidate::config ( $config, $this->mongoKeys );


        // Mongo connection
        $mongoServer = $config['mongoServer'];
        $mongoPort = $config['mongoPort'];
        $manager = new \MongoDB\Driver\Manager("mongodb://$mongoServer:$mongoPort");

        // DB name
        $mongoDBName = $config['mongoDBName'];
        // Collection name
        $mongoCollectionName = $config['mongoCollectionName'];

        // Bulk Write
        $bulk = new \MongoDB\Driver\BulkWrite;

        // Mongo ReadPreference (Route to primary member of a replica set)
        // RP_PRIMARY is default
        $readPreference = new \MongoDB\Driver\ReadPreference(
            \MongoDB\Driver\ReadPreference::RP_PRIMARY
        );
        // Write concern
        $writeConcern = new \MongoDB\Driver\WriteConcern(
            \MongoDB\Driver\WriteConcern::MAJORITY,
            1000
        );

        // Array to process
        $arrayToProcess = $config['arrayToProcess'];

        $fileToProcess = $config['fileToProcess'];

        $fileNameArray = explode('/',$fileToProcess);
        $fileName = end($fileNameArray);

        // Cycle on catalogue-items (products)
        foreach ($arrayToProcess as $i => $aCatalogueItem) {

            // Product GTIN
                $gtin = $aCatalogueItem['catalogueItem'][ 'tradeItem']['gtin'];
                if(empty($gtin)) {
                    $message = "tradeItem-gtin is empty in: \n"
                        . var_export($aCatalogueItem, true);
                    throw new Exception($message);
                }

            $bulk->update(
              ['catalogueItem.tradeItem.gtin' => $gtin],
              ['$set' => $aCatalogueItem],
              ['multi' => false, 'upsert' => true]
          );
        }// foreach $arrayToProcess

        // Execute Bulk Write
        try{
            $result = $manager->executeBulkWrite("$mongoDBName.$mongoCollectionName", $bulk, $writeConcern);
        }catch (\Exception $e){
            $log = new Logging();
            $log->lfile('Error.log');
            // write message to the log file
            $log->lwrite($fileName.'--------'.$e.'');
            $log->lclose();
            return false;
        }

        // return
        return $result;
    }



    function HandleXmlError($errno, $errstr, $errfile, $errline)
    {
        if ($errno==E_WARNING && (substr_count($errstr,"DOMDocument::loadXML()")>0))
        {
            throw new DOMException($errstr);
        }
        else
            return false;
    }

}
