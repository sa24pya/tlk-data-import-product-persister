<?php
/**
 * File: tests/ProductPersisterTest.php
 *
 * @author Victor
 *
 * @package DataImportProductPersister
 * @subpackage ProductPersister
 * @version 1.0.1
 *
 */


namespace DataImportProductPersister;

use DataImportProductPersister\ProductPersister;
use Core\Mongo\CollectionCreator;

// .env
use Dotenv\Dotenv;



/**
 * Class XMLCatalogueItemExtractor
 *
 * @author Victor
 *
 * @package: DataImportProductPersister
 * @subpackage ProductPersister
 * @version: 1.0.1
 */
class ProductPersisterTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $dotenv = new Dotenv(dirname( __DIR__));
        $dotenv->load();
    }

    public function tearDown()
    {
    }

    public function testUpdate()
    {
        $docCollection = array();
        for ( $i = 0 ; $i < 10 ; $i++ ) {
            $docCollection[] = $this->createDocuments( $i );
        }

        $pp = new ProductPersister();
        $config = array(
            'mongoServer' => getenv('MONGO_SERVER'),
            'mongoPort' => getenv('MONGO_PORT'),
            'mongoDBName' => getenv('MONGO_DB_NAME'),
            'mongoCollectionName' => getenv('MONGO_COLLECTION_NAME'),
            'arrayToProcess' => $docCollection,
        );
        try{
            $r = $pp->persist($config);
        }
        catch (\Exception $e) {
            echo $e->getMessage() . ' L:' . $e->getLine();
        }
        $this->assertTrue(true);
    }


    public function testInsert()
    {
        $manager = new \MongoDB\Driver\Manager("mongodb://localhost:27017");
        $createCollection = new CollectionCreator("cappedCollection");
        $createCollection->setCappedCollection(64 * 1024);

        $this->assertTrue(true);
    }


    /**
     * Create some documents (array)
     */
    private function createDocuments( $i )
    {
        $a = array(
            'name' => 'BASF C90' ,
            'GTIN' => $i,
            'price' => 19.1,
            'stock' => 1,
            'status' => 'enabled',
            'GLN' => $i,
            'image' => 'image01.jpg',
            'description' => 'Compact Cassette BASF C90',
            'brandName' => 'BASF',
            'tradeItem' => array(
                'name' => 'TAPE BASF C90',
                'GTIN' => $i + 100,
                'price' => 1.9,
                'stock'=> 2,
                'status' => 'enabled',
                'GLN' => $i + 100,
                'image' => 'image02.jpg',
                'description' => 'TAPE Compact Cassette BASF C90',
                'brandName'=> 'BASF',
                'tradeItem' => array(
                    'name' => 'MAGNETE TAPE BASF C90',
                    'GTIN' => $i + 1000 ,
                    'price' => 0.1 ,
                    'stock' => 3 ,
                    'status' => 'enabled',
                    'GLN' => $i + 1000 ,
                    'image' => 'image03.jpg',
                    'description' => 'MAGNETE TAPE Compact Cassette BASF C90',
                    'brandName'=> 'BASF',
                )
            )
        );
        return $a;
    }
}